# Configuration Magic for Ruby on Rails

This simple gem tries to make it easy to configure a Ruby on Rails application.

Currently:

* Use multiple YAML files, with inheritance.

In the future:

* Use ENV if available.
* Use database override if set.
* UI for database settings.

## Usage

`Conf.load` will load the files specified in order, later files overriding the earlier files.

If you want to be able to use `Conf` everywhere, put it very early in `config/application.rb`.

`config/application.rb`:
    require 'conf'
    Conf.load(
      File.expand_path('../defaults.yml', __FILE__),
      File.expand_path('../config.yml', __FILE__)
    )
